import React from "react";
import ToDoInput from "./components/ToDoInput";
import ToDoList from "./components/ToDoList";
import ToDoCounter from "./components/ToDoCounter";

function App() {
  return (
    <div className="container-fluid">
      <h1>To Do</h1>
      <ToDoCounter />
      <ToDoInput />
      <ToDoList />
    </div>
  );
}

export default App;
