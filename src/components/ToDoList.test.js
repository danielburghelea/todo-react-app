import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import ToDoList from './ToDoList';
import '@testing-library/jest-dom';
import { ToDoContext } from '../contexts/TodoContext';

const mockData = {
  todoList: [
    {
      id: 1,
      title: 'Task 1',
      body: 'Task 1 description',
      completed: false,
    },
    {
      id: 2,
      title: 'Task 2',
      body: 'Task 2 description',
      completed: true,
    },
  ],
};

const mockDispatch = jest.fn()

describe('ToDoList', () => {
  
  test('renders todo list items', () => {
    render(
      <ToDoContext.Provider value={{ data: mockData, dispatch: mockDispatch }}>
        <ToDoList />
      </ToDoContext.Provider>
    );

    const todoItems = screen.getAllByRole('listitem');
    expect(todoItems).toHaveLength(2);

    expect(todoItems[0]).toHaveTextContent('Task 1');
    expect(todoItems[0]).toHaveTextContent('Task 1 description');

    expect(todoItems[1]).toHaveTextContent('Task 2');
    expect(todoItems[1]).toHaveTextContent('Task 2 description');
    expect(todoItems[1]).toHaveClass('done');
  });

  test('removes todo when delete button is clicked', () => {
    render(
      <ToDoContext.Provider value={{ data: mockData, dispatch: mockDispatch }}>
        <ToDoList />
      </ToDoContext.Provider>
    );

    let todoItems = screen.getAllByRole('listitem');
    expect(todoItems).toHaveLength(2);

    const deleteButton = screen.getAllByRole('button', { name: 'X' })[0];
    fireEvent.click(deleteButton);

    expect(mockDispatch).toHaveBeenCalledWith({
      id: 1,
      type: "REMOVE_TODO"
    });
  });

  test('calls toggleToDo when checkbox is clicked', () => {
    render(
      <ToDoContext.Provider value={{ data: mockData, dispatch: mockDispatch }}>
        <ToDoList />
      </ToDoContext.Provider>
    );

    const checkbox = screen.getAllByRole('checkbox')[1];
    fireEvent.click(checkbox);

    expect(mockDispatch).toHaveBeenCalledWith({
      type: 'TOGGLE_TODO',
      id: 2,
    });

  });
});
