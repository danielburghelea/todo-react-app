import React, { useState, useContext } from "react";
import { ToDoContext } from "../contexts/TodoContext";
import actionTypes from "../reducers/actionTypes";

const ToDoInput = () => {
  const [toDoTitle, setToDoTitle] = useState("");
  const [toDoBody, setToDoBody] = useState("");
  const { dispatch } = useContext(ToDoContext);

  const handleChangeTitle = (e) => {
    setToDoTitle(e.target.value);
  };

  const handleChangeBody = (e) => {
    setToDoBody(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({ type: actionTypes.ADD_TODO, title: toDoTitle, body: toDoBody });
    setToDoTitle("");
    setToDoBody("")
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={toDoTitle}
        onChange={handleChangeTitle}
        placeholder="Title"
        required
      />
      <input
        type="text"
        value={toDoBody}
        onChange={handleChangeBody}
        placeholder="Body"
        required
      />

      <input type="submit" value="Add" />
    </form>
  );
};

export default ToDoInput;
