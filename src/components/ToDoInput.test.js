import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import ToDoInput from './ToDoInput';
import { ToDoContext } from "../contexts/TodoContext";
import '@testing-library/jest-dom';

describe('ToDoInput', () => {
  test('submits the form with the entered values', () => {
    const mockDispatch = jest.fn();

    render(
      <ToDoContext.Provider value={{ dispatch: mockDispatch }}>
        <ToDoInput />
      </ToDoContext.Provider>
    );

    const titleInput = screen.getByPlaceholderText('Title');
    const bodyInput = screen.getByPlaceholderText('Body');
    fireEvent.change(titleInput, { target: { value: 'Task 1' } });
    fireEvent.change(bodyInput, { target: { value: 'Task 1 description' } });

    const addButton = screen.getByRole('button', { name: 'Add' });
    fireEvent.click(addButton);

    expect(mockDispatch).toHaveBeenCalledWith({
      type: 'ADD_TODO',
      title: 'Task 1',
      body: 'Task 1 description',
    });

    expect(titleInput.value).toBe('');
    expect(bodyInput.value).toBe('');
  });
});
