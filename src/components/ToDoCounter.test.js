import React from 'react';
import { render, screen } from '@testing-library/react';
import ToDoCounter from './ToDoCounter';
import { ToDoContext } from '../contexts/TodoContext';
import '@testing-library/jest-dom';

describe('ToDoCounter', () => {
  test('renders the correct count of completed and todo items', () => {
    const mockData = {
      todoList: [
        { id: 1, title: 'Task 1', completed: false },
        { id: 2, title: 'Task 2', completed: true },
        { id: 3, title: 'Task 3', completed: false },
      ],
    };

    render(
      <ToDoContext.Provider value={{ data: mockData }}>
        <ToDoCounter />
      </ToDoContext.Provider>
    );

    const completedCount = screen.getByText(/Done: (\d+)/i);
    expect(completedCount).toHaveTextContent('Done: 1');

    const todoCount = screen.getByText(/ToDo: (\d+)/i);
    expect(todoCount).toHaveTextContent('ToDo: 2');
  });
});
