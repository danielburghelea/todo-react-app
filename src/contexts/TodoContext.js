import React, { createContext, useReducer } from "react";
import { toDoReducer } from "../reducers/toDoReducer";

const initialState = {
  todoList: []
};

export const ToDoContext = createContext();

const ToDoContextProvider = ({ children, defaultData }) => {
  const [data, dispatch] = useReducer(toDoReducer, defaultData || initialState);

  return (
    <ToDoContext.Provider value={{ data, dispatch }}>
      {children}
    </ToDoContext.Provider>
  );
};

export default ToDoContextProvider;